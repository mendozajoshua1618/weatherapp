package weather;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WeatherAppController {

	@Autowired
	private WeatherRepository weatherRepository;
   	
	private ArrayList<WeatherLog> dbLogs = new ArrayList<WeatherLog>();
	private final int MAXLOG = 5;
	
    @GetMapping("/check-weather")
	@ResponseBody
	public WeatherCheckResponse giveWeatherStatus() {
    	WeatherCheckResponse wcr = new WeatherCheckResponse();
    	logResponse(wcr);
    	return wcr;
	}

	private void logResponse(WeatherCheckResponse wcr) {
		for (WeatherStatus ws : wcr.getWeatherInfoList()) {
			dbLogs = (ArrayList<WeatherLog>) weatherRepository.findAll();
			if(isStatusUnique(ws)) {
				trimDBto(MAXLOG-1);
				storetoDB(new WeatherLog(ws));	
			}
		}		
	}

	private void trimDBto(int max) {
		if (dbLogs.size() <= max) {
			return;
		}
		weatherRepository.deleteById(getEarliestLogId());
	}

	private Long getEarliestLogId() {
		Long[] logIds = copyLogIds();
		
		for (int i = 0; i < MAXLOG-1; i++) {
			if (logIds[i] < logIds[i+1]) {
				long temp = logIds[i];
				logIds[i] = logIds[i+1];
				logIds[i+1] = temp;
			}		
		}
		return logIds[MAXLOG-1];
	}

	private Long[] copyLogIds() {
		Long[] logIds = new Long[MAXLOG];
		int index = 0;

		for (WeatherLog wl : dbLogs) {
			logIds[index++] = wl.getId();
		}
		return logIds;
	}
	
	private boolean isStatusUnique(WeatherStatus ws) {
		for (WeatherLog wl : dbLogs) {
			if(wl.hasMatching(ws)) {
				return false;
			}
		}
		return true;
	}
	
	private void storetoDB(WeatherLog weatherLog) {
		weatherRepository.save(weatherLog);	
	}
}
