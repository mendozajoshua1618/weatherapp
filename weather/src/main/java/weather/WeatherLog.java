package weather;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import net.aksingh.owmjapis.AbstractWeather.Weather;

@Entity
public class WeatherLog {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@GeneratedValue(strategy=GenerationType.AUTO)
	private String responseId;	
	private String location;
	private String actualWeather;
	private String temperature;
	private Timestamp dtimeInserted;
	
	public WeatherLog() {}
	
	public WeatherLog(WeatherStatus ws) {
		this.location = ws.getLocation();
		this.actualWeather = toWeatherLogString(ws.getActualWeather());
		this.temperature = ws.getTemp();
		this.dtimeInserted = new Timestamp(System.currentTimeMillis());
	}

	private String toWeatherLogString(Weather w) {
        return "weatherIconName:" + w.getWeatherIconName()
             + ",weatherDescription:" + w.getWeatherDescription()
             + ",weatherName:" + w.getWeatherName()
             + ",weatherCode:" + w.getWeatherCode();
	}

	public Long getId() {
		return id;
	}
	public String getResponseId() {
		return responseId;
	}
	public Timestamp getDtimeInserted() {
		return dtimeInserted;
	}
	public String getLocation() {
		return location;
	}
	public String getActualWeather() {
		return actualWeather;
	}
	public String getTemperature() {
		return temperature;
	}

	public boolean hasMatching(WeatherStatus ws) {
		return ws.getLocation().equals(this.location)
				&& toWeatherLogString(ws.getActualWeather()).equals(this.actualWeather)
				&& ws.getTemp().equals(this.temperature);
	}
}
