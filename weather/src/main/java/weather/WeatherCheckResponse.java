package weather;

import java.util.ArrayList;

import net.aksingh.owmjapis.OpenWeatherMap;

public class WeatherCheckResponse {	
	private final String OWM_AP_KEY= "99acf951921462b4a7908b8f32b8e0a7";
	private OpenWeatherMap owm = new OpenWeatherMap(OpenWeatherMap.Units.METRIC, OWM_AP_KEY);	
		
	private final long LONDON_KEY = 2643743;
	private final long PRAGUE_KEY = 3067696;
	private final long SANFRAN_KEY = 5391959;
	
	private ArrayList<WeatherStatus> weatherStatusList = new ArrayList<WeatherStatus>();
	
	public WeatherCheckResponse() {
		weatherStatusList.add(new WeatherStatus(owm, LONDON_KEY));
		weatherStatusList.add(new WeatherStatus(owm, PRAGUE_KEY));
		weatherStatusList.add(new WeatherStatus(owm, SANFRAN_KEY));	
	}
		
	public ArrayList<WeatherStatus> getWeatherInfoList() {
		return weatherStatusList;
	}
}
