package weather;

import net.aksingh.owmjapis.AbstractWeather.Weather;
import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

public class WeatherStatus {
	private String location;
	private Weather actualWeather;
	private String temperature;

    public WeatherStatus(OpenWeatherMap owm, long cityCode) {
	try {
		CurrentWeather cw = owm.currentWeatherByCityCode(cityCode);			
		this.location = cw.getCityName();
		this.actualWeather = cw.getWeatherInstance(0);
		this.temperature = String.valueOf(cw.getMainInstance().getTemperature());			
		} catch (Exception e) {		
			e.printStackTrace();
		}
	}
	
	public String getLocation() {
		return this.location;
	}
	
	public Weather getActualWeather() {
		return this.actualWeather;
	}
	
	public String getTemp() {
		return this.temperature;
	}
}
